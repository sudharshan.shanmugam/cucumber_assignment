package StepFunction;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class TestDefinition {

	 public static WebDriver driver =null;

@Given("openbrowser")
public void openbrowser() {
	
	driver =new ChromeDriver();
	driver.get("https://demowebshop.tricentis.com/");
	driver.manage().window().maximize();
	driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
	}

@Then("The Home page of the website get displayed")
public void the_home_page_of_the_website_get_displayed() {
	
}

@When("Click Login profile  button")
public void click_login_profile_button() {
	
	driver.findElement(By.linkText("Log in")).click();

    }

@When("Enter the email {string} in the email textbox.")
public void enter_the_email_in_the_email_textbox(String string) {
	driver.findElement(By.id("Email")).sendKeys(string);

    }

@When("Enter the password {string} in the password textbox.")
public void enter_the_password_in_the_password_textbox(String string) {
	driver.findElement(By.id("Password")).sendKeys(string);

    }

@When("Click on the login button.")
public void click_on_the_login_button() {
	   driver.findElement(By.xpath("//input[@value='Log in']")).click();

    }

@Then("The home page with logged username should be displayed.")
public void the_home_page_with_logged_username_should_be_displayed() {
    }

@Then("Click the logout button.")
public void click_the_logout_button() {
	driver.findElement(By.linkText("Log out")).click();

   
}

@Then("The user should taken to the homepage of the demo web shop.")
public void the_user_should_taken_to_the_homepage_of_the_demo_web_shop() {
}

@Then("Close the browser.")
public void close_the_browser() 
{
	driver.quit();

}

}
