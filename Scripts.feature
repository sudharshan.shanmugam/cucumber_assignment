Feature: Demo Web Shop login functions

Scenario Outline: Demo web shop logins verify

  Given openbrowser 
   Then The Home page of the website get displayed
   When Click Login profile  button
   And Enter the email "<USERNAME>" in the email textbox.
   And Enter the password "<PASSWORD>" in the password textbox.
   And  Click on the login button.
   Then The home page with logged username should be displayed.
    And Click the logout button.
    Then The user should taken to the homepage of the demo web shop.
    And Close the browser.
    
    
    Examples:
    
        | USERNAME                   | PASSWORD    |
        | sudhar3298@gmail.com       | Sudhar@4200 |
        | sudhar3298@gmail.com       | Sudhar@4200 |
    